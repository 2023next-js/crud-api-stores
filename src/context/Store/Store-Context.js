/*el contexto permite compartir un estado con toda la aplicación 
y el archivo "Store-Context.js" contendrá el contexto y un contenedor para compartir los datos*/
import {createContext} from "react"

const StoreContext = createContext()

export default StoreContext