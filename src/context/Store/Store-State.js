import React, {useReducer} from 'react'
import StoreReducer from '@/src/context/Store/Store-Reducer'
import StoreContext from '@/src/context/Store/Store-Context'
import axios from 'axios'
const StoreState = (props) =>{
    //variables de estado incial, 
    const initialState = {
        storesAll: [],
        productAll: []
    }
    /*state nombre del estado del componente
      Dispatch se encarga de enviar la actualizacion al metodo reducer
    CharacterReducer va retornar el nuevo estado
    */
  const [state, dispatch] = useReducer(StoreReducer, initialState)
    const getAllStore = async () => {
        try {
            const res = await axios.get('api/stores')
            const data = await res.data

            dispatch({
                type: 'GET_STORE',
                payload: data
            })
        } catch (error) {
            console.error(error)
        }
    }
    //funcion para mostrar todos los productos
    const getIDStore = async (id) =>{
        try {
            const res = await axios.get('api/stores')
            const dataStores = await res.data
            console.log(id+'id')
            dispatch({
                type: 'GET_PRODUCT',
                payload: id,dataStores
            })
        } catch (error) {
            console.error(error)
        }
    }
    return(
        <StoreContext.Provider value={{
         storesAll: state.storesAll,
         productAll: state.productAll,
         getAllStore,
         getIDStore,
   
        }}>
            {props.children}
        </StoreContext.Provider>
    )
}
export default StoreState