import {GET_STORE, GET_PRODUCT} from '@/src/context/functions'
/*default function  va devolver un nuevo estado.
state, estado actual.
action, objeto que envia el metodo dispatch.
*/
export default (state, action) =>{
    //desctructuramos action 
    //const {payload, type, characters,info, id} = action
    const {payload,type,dataStores} = action
    /* Metodos que retornan la actualizacion del estado.
    Esto es útil para restablecer el estado más tarde en respuesta to in action*/
    switch(type){
        case GET_STORE:
            return {
                ...state,
                storesAll: payload
            }
        case GET_PRODUCT:
            console.log(dataStores)
            console.log(payload)
            const resProduct = dataStores.find(item => item.nit == payload)
            console.log('alldaa---'+resProduct)
            return{
                ...state,
                productAll: resProduct
            }
        default:
            return state
    }
}
