import Nav from './Nav'
import styles from '@/src/styles/header.module.css'
const Header = () => {
  return (
      <div className={`tdBttn ${styles.divHeader}`} > 
        <div ><h1>Crud Tiendas</h1></div>
        <div><Nav/></div>
      </div>
  )
}
export default Header
