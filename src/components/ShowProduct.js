import { HiTrash } from "react-icons/hi2"
import styles from '@/src/styles/showAll.module.css'
import Link from "next/link"
import {TbEdit}from "react-icons/tb"
const ShowProduct = ({ prod, price, nit}) => {
  const deleteProduct = async (product) => {
    await fetch(`api/stores`,{
              method: 'DELETE',
              body: JSON.stringify(product)
            })}
    return ( 
    <div>
      <table>
        <thead>
          <tr>
            <th scope="col">Productos</th>
            <th scope="col">Precios</th>
            <th scope="col">Acciones</th>
          </tr>
        </thead>
        <tbody>
          {prod.map((e, index) => (
            <tr key={index}>  
              <th>{e}</th>
              <th>{price[index]}</th>
              <th >
              <div className={`tdBttn ${styles.tdBtton}`}>
                <button
                  onClick={() => deleteProduct(e)}
                  className="secondary"
                >
                  <HiTrash />
                </button>
              </div>
              </th>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  )
}
export default ShowProduct
