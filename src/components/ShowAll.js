
import {HiTrash} from "react-icons/hi2"
import {TbEdit} from "react-icons/tb"
import {BsListUl} from 'react-icons/bs'
import Link from "next/link"
import styles from '@/src/styles/showAll.module.css'
const ShowStores = ({ storesAll }) => {

  const deleteStore = async (id) => {
    await fetch(`api/stores`,{
              method: 'DELETE',
              body: JSON.stringify(id)
            })
  }
  return (
    <div>
      <table>
        <thead>
          <tr>
            <th scope="col">Nit</th>
            <th scope="col">Nombre</th>
            <th scope="col"># Productos</th>
            <th scope="col">Acciones</th>
          </tr>
        </thead>
        <tbody>
          {storesAll.map((e) => (
            <tr>
              <th scope="row">{e.nit}</th>
              <td>{e.name}</td>
              <td>{e.product ? Object.keys(e.product).length : 0}</td>
              <td>
                <div className={`tdBttn ${styles.tdBtton}`}>
              <Link href={`/all_Product?id=${e.nit}`} as={`/all_Product?id=${e.nit}`} >
                <BsListUl/>
                </Link>
              <Link href={`/UpStore?id=${e.nit}`} as={`/UpStore?id=${e.nit}`}  >
                <TbEdit/>
              </Link>
                <button
                  onClick={() => deleteStore(e.nit)}
                  className="secondary">
                  <HiTrash/>
                </button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  )
}

export default ShowStores
