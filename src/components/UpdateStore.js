import styles from '@/src/styles/store.module.css'

const UpStore = ({storeID}) => {
   const {nit, name} = storeID
  const handle = (e) => {
    e.preventDefault()
    const formStore = {
      nit: nit,
      name: e.target.nameStore.value,
    }
    fetch("api/stores", {
      method: "PUT",
      body: JSON.stringify(formStore),
    })
    console.log(formStore)
  }

  return (
    <form action="" onSubmit={handle}>
      <fieldset className={`${styles.fieldSet}`}>
      <label>
          Nit:
          <input
            type="number"
            name="numberNit"
            defaultValue={`${nit}`}
            disabled
          ></input>
        </label>
        <label>
          Nuevo nombre de la tienda:
          <input
            type="text"
            name="nameStore"
            placeholder="Nuevo nombre de la tienda"
            defaultValue='REACTjs'
          ></input>
        </label>
        </fieldset>
        <div className={`${styles.divAgregar}`}>
        <input type="submit" value="Actualizar" className='dInput'></input>
        </div>
    </form>
  )
}

export default UpStore
