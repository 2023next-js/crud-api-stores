import styles from '@/src/styles/product.module.css'

const AddProduct = () => {
  let contador = 0
  const addNewProduct = (e) => {
    e.preventDefault()
    let contenedor = document.getElementById("nuevo_Producto")
    contador++

    let _div = `<div id="div${contador}">
    <label>
      <div> Producto:</div>
    <input  
          type="text"
          name="nameProduct"
          placeholder="Ingrese nombre del Producto" >
    </input>
  </label>
  <label>
      <div> Precio:</div>
    <input  
          type="number"
          name="namePrice"
          placeholder="Ingrese nombre del Producto" >
    </input>
  </label>
  </div>`

    contenedor.innerHTML += _div
  }
  const handle = (e) => {
    e.preventDefault()
    const inputs = document.querySelectorAll("input:not([type='submit'])")
    let Product = []
    let allPrice = []
    inputs.forEach((input) => {
      if (input.name == "nameProduct") {
        Product.push(input.value);
      } else if (input.name == "namePrice") {
        allPrice.push(input.value);
      }
    })
    let contentProduct = [{ product: Product, price: allPrice }]

    const formProduct = {
      nit: e.target.numberNit.value,
      productOnly: contentProduct,
    }
    fetch("api/stores", {
      method: "PUT",
      body: JSON.stringify(formProduct),
    })
   // console.log(formProduct)
  }
  return (
    <>
      <form action="" onSubmit={handle}>
      <fieldset className={`${styles.divI} `}>
          <label>
            Nit:
            <input
              type="number"
              name="numberNit"
              placeholder="Ingrese nit de la tienda"
              defaultValue="12323"
            ></input>
          </label>
          <label>
            Producto:
            <input
              type="text"
              name="nameProduct"
              placeholder="Ingrese nombre del Producto"
              defaultValue="Papas"
            ></input>
          </label>
          <label>
            Precio:
            <input
              type="number"
              name="namePrice"
              placeholder="Ingrese el precio del producto"
              defaultValue="98"
            ></input>
          </label>
          <div id="nuevo_Producto"> </div>
          </fieldset>
          <div className={`${styles.divI} ${styles.tdBtton}`}>
            
          <button type="button" onClick={addNewProduct} className={`${styles.dInput} `}>
            {" "}
            Agregar + Producto
            {" "}
          </button>
          <input type="submit" value="Agregar" className={`${styles.dInput} `}></input>
          </div>
          
      </form>
    </>
  )
}

export default AddProduct
