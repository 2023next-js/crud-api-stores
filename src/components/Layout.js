import Head from 'next/head'
import Header from './Header'


const layout = ({children, title, description}) =>{
    return (
        <>
            <Head>
                <title>{title}</title>
                <meta name='description' content={description} />
            </Head>
            <Header />
            {children}
        </>
    )
}

export default layout
