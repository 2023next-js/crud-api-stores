import Link from "next/link"

const Nav = () => {
  return (
        <nav>
          <ul>
            <li><Link href="/">Inicio</Link></li>
            <li><Link href="/store">Agregar Tienda</Link></li>
            <li><Link href="/all_Stores">Ver Tiendas</Link></li>
          </ul>
        </nav>
  )
}
export default Nav