import styles from '@/src/styles/store.module.css'

const AddStore = () => {
  const handle = (e) => {
    e.preventDefault()
    const formStore = {
      nit: e.target.numberNit.value,
      name: e.target.nameStore.value,
    }
    
    fetch("api/stores", {
      method: "POST",
      
      body: JSON.stringify(formStore),
    })
    console.log(formStore)
  }

  return (
    <form action="" onSubmit={handle}>
      <fieldset className={`${styles.fieldSet}`}>
      <label>
          Nit:
          <input
            type="number"
            name="numberNit"
            placeholder="Ingrese nit de la tienda"
            defaultValue="12323"
          ></input>
        </label>
        <label>
          Nombre de la tienda:
          <input
            type="text"
            name="nameStore"
            placeholder="Ingrese nombre de la tienda"
            defaultValue="D1|TIENDA"
          ></input>
        </label>
        </fieldset>
        <div className={`${styles.divAgregar}`}>
        <input type="submit" value="Agregar"></input>
        </div>

    </form>
  )
}

export default AddStore
