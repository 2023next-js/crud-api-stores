const UpProduct = ({ nit }) => {
  const handle = (e) => {
    e.preventDefault();
    const inputs = document.querySelectorAll("input:not([type='submit'])");
    let allProduct = []
    let allPrice = []
    let newProduct = []
    inputs.forEach((input) => {
      switch (input.name) {
        case "nameProduct":
         allProduct.push(input.value)
         return 
        case "namePrice":
          allPrice.push(input.value)
          return 
        case "nameNewProduct":
          newProduct.push(input.value)
          return 
        default:
          return "No se realizaron los cambios";
      }
    })
    let contentProduct = [
      { product: allProduct, price: allPrice, newProduct: newProduct },
    ];

    const formProduct = {
      nit: nit,
      productOnly: contentProduct,
    };
    fetch("api/stores", {
      method: "PUT",
      body: JSON.stringify(formProduct),
    });
    // console.log(formProduct)
  };
  return (
    <>
      <form action="" onSubmit={handle}>
        <fieldset>
          <label>
            Producto Anterior:
            <input
              type="text"
              name="nameProduct"
              placeholder="Nombre del Producto anterior"
              defaultValue="Papas"
            ></input>
          </label>
          <label>
            Nuevo Producto:
            <input
              type="text"
              name="nameNewProduct"
              placeholder="Ingrese  nombre del Producto"
              defaultValue="Yuca"
            ></input>
          </label>
          <label>
            Precio:
            <input
              type="number"
              name="namePrice"
              placeholder="Ingrese el precio del producto"
              defaultValue="9999"
            ></input>
          </label>
        </fieldset>
        <div className="divI">
          <input type="submit" value="Actualizar" className="dInput"></input>
        </div>
      </form>
    </>
  );
};

export default UpProduct;
