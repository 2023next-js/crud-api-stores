const Stores = [
  {
    id: 1,
    nit: 34,
    name: "d1",
    product: {
    papitasBQ: 3500,
    papitasLimon: 3200,
    mani: 800,
    cafe: 8000,
    azucar: 3500,
    papel: 3000
  }
},
  {
    id: 2,
    nit: 3312,
    name:  "olimpica",
    product: {
    papitasBQ: 3600,
    papitasPollo: 3500,
    mani: 850,
    cafe: 8010,
    azucar: 3200,
    papel: 3500
  }
},
  {
    id: 3,
    nit: 312,
    name: "exito",
    product: {
    papitasBQ: 3650,
    papitasLimon: 3200,
    mani: 790,
    cafe: 8000,
    azucar: 3500,
    papel: 3000,
    gomitas: 4520
  }},
  {
    id: 5,
    nit: 324,
    name: "isimo",
    product: {
    papitasBQ: 3700,
    papitasLimon: 3200,
    cafe: 7000,
    azucar: 3000,
    papel: 3200,
    gomitas: 4320
  }
},
  {
    id: 6,
    nit: 34312,
    name: "otra",
    product: {
     papitasPollo: 3700,
      papitasBQ: 3200,
      cafe: 7000,
      mani: 3000,
      gomitas: 4320
  }
}
]

export default Stores
