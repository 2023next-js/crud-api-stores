import stores from '@/src/db_stores/data.js'

 class Crud {
getShowStores(){
    return  stores
}
  maxID = () => {
    let id = stores.reduce((max, student) => {
      return student.id > max ? student.id : max;
    }, 0)
    //Encuentra el id mayor
    return id + 1
  }
  
//  getIDStore(id = 1){
//     return stores.find(element => element.id === id)
// }

postNewStore(newStore){
  const {nit, name} = newStore
  const foundNit = stores.find(store => store.nit === Number(nit))
  if (foundNit) {
    return { error: 'NIT already exists' };
  }
  let id= this.maxID()
  stores.push({id:id,nit: Number(nit), name:name})
  return stores
}

putInfoStore(newStore){
  const { nit, name} = newStore
  const foundNit = stores.find(store => store.nit == Number(nit))
  console.log(name)
  if (!foundNit) {

    return { error: 'NIT already exists PUTT' }
  }
 
  foundNit.name = name

  return stores
}
putStore(newStore) {
  const { nit, productOnly } = newStore
  const foundNit = stores.findIndex(item => item.nit === Number(nit))
  if (foundNit === -1) {
    return { error: 'NIT not found' }
  }  
  const [product] = productOnly
  const { product: products, price} = product
  const updatedStore = stores[foundNit]
  if (!updatedStore.product) {
    updatedStore.product = {}
  } 
  products.forEach((productName, index) => {
    if (!updatedStore.product.hasOwnProperty(productName)) {
      updatedStore.product[productName] = Number(price[index])
      return
    }
  })

  stores[foundNit] = updatedStore
  return stores
}

deleteProduct (prod ) {
  if (!prod ) {
    return { error: 'Producto not found' }
  }  

  const index = stores.findIndex(store => store.product && store.product.hasOwnProperty(prod))
  if(index !== -1){
    stores[index].product = { ...stores[index].product }//copia del producto 
    delete stores[index].product[prod]// Elimina el producto especificado junto con su valor
    return stores[index]
  }
  return stores
}

deleteStore (id ) {
    const index = stores.findIndex((store)=> store.nit === id) 
    stores.splice(index,1)//Elimina la tienda espeficicada del store
      return stores
    }   
}

export default  Crud 