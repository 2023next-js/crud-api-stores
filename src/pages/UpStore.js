import Layout from "@/src/components/Layout"
import Link from "next/link"
import UpdateStore from '@/src/components/UpdateStore'
import { useContext, useEffect} from "react"
import StoreContext from '@/src/context/Store/Store-Context'
import {useRouter} from "next/router"

const UpStore = ()=> {
  const {productAll, getIDStore}= useContext(StoreContext)
  const router = useRouter()
  const {id} = router.query
// console.log(router.query + id )

  useEffect(()=>{
    if(id){
      getIDStore(id)
    }
  },[])
  const {name} = productAll
  return(
    <Layout title="Update-Store" description="Update Store">
      <div className='divH1_form'>
      <h1>Update Store - {`${name}`}</h1>
      <Link href={`/all_Stores`}>Regresar</Link>
      <UpdateStore storeID={productAll}/>
      </div>
  </Layout>
  )
}

export default UpStore