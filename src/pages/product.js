import Layout from "@/src/components/Layout"
import AddProduct from "@/src/components/AddProduct"
import styles from '@/src/styles/product.module.css'
import Link from "next/link"
const Product = ()=> {

  return(
    <Layout title="Product" description="Add Product">
      <div className={`${styles.divH1} `}>
      <h1>Agregar Producto</h1>
      <Link href={`/all_Product`} > Regresar</Link>
      <AddProduct />
      </div>
  </Layout>
  )

}

export default Product