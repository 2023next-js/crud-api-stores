import Layout from "@/src/components/Layout"
import Link from "next/link"
import UpdateProduct from '@/src/components/UpdateProduct'
import Results from '@/src/components/Results'
import {useRouter} from "next/router"
import { useContext, useEffect} from "react"
import StoreContext from '@/src/context/Store/Store-Context'
const UpProduct = ()=> {
  const {productAll, getIDStore}= useContext(StoreContext)
  const router = useRouter()
  const {id} = router.query
// console.log(router.query + id )

  useEffect(()=>{
    if(id){
      getIDStore(id)
    }
  },[])
  return(
    <Layout title="Update-Product" description="Update Product">
      <div className='divH1_form'>
      <h1>Update Product</h1>
      <Link href={`/all_Product`}>Regresar</Link>
      <Results storeID={productAll} Page={UpdateProduct}/>
      </div>
  </Layout>
  )

}

export default UpProduct