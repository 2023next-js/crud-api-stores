import Layout from "@/src/components/Layout"
import ShowAll from '@/src/components/ShowProduct'
import { useContext, useEffect} from "react"
import StoreContext from '@/src/context/Store/Store-Context'
import {useRouter} from "next/router"
import Link from 'next/link'
import styles from '@/src/styles/product.module.css'
import Results from "../components/Results"

const showProducts = () => {
  const {productAll, getIDStore}= useContext(StoreContext)
  const router = useRouter()
  const {id} = router.query
  useEffect(()=>{
    if(id){
      getIDStore(id)
    }
  },[])

  return (
    <Layout title="Tienda" description="Create Store">
      <div className={`divH1_form `}>
      <h1>Tabla de Productos de {productAll.name}</h1>
      <div>
        <div className={`tdBttn ${styles.divAction}`}>
      <Link href={`/all_Stores`} > Regresar</Link>
      <Link href={`/product`} >Agregar Producto</Link>
      </div>
      <Results storeID={productAll} Page={ShowAll}/>
      </div>
      </div>
    </Layout>
  )

}
export default showProducts 