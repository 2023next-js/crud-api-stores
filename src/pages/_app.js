import '@/src/styles/globals.css'
import StoreState from '@/src/context/Store/Store-State'

function MyApp ({Component, pageProps}){
    return(
        <StoreState>
            <Component {...pageProps} />
        </StoreState>
    )
}

export default MyApp
