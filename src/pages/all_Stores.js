import Layout from "@/src/components/Layout"
import ShowAll from '@/src/components/ShowAll'
import { useContext, useEffect} from "react"
import StoreContext from '@/src/context/Store/Store-Context'
import styles from '@/src/styles/showAll.module.css'

const showStores = () => {
  const {storesAll, getAllStore}= useContext(StoreContext)
  useEffect(()=>{
    getAllStore()
  },[])
  return (
    <Layout title="Tienda" description="Create Store">
      <div className={`divH1_form ${styles.divH1}`}>
      <h1>Tabla de Tiendas</h1>
      <ShowAll storesAll={storesAll}/>
      </div>
    </Layout>
  )
}
export default showStores 
