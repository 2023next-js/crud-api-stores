import Layout from "@/src/components/Layout"
import AddStore from "@/src/components/AddStore"
const Store = () => {

  return (
    <Layout title="Tienda" description="Create Store">
      <div className='divH1_form'>
        <h1>Agregar Tienda</h1>
      <AddStore />
      </div>
    </Layout>
  )

}
export default Store