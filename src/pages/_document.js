import {Html, Head, Main, NextScript} from "next/dist/pages/_document";

export default function Document(){
    return(
        <Html>
            <Head>
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@picocss/pico@1/css/pico.min.css" />  
            </Head> 
            <body>
                <Main />
                <NextScript />
            </body>
        </Html>
    )
}
