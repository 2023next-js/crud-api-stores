import Crud from "@/src/function/crud";

const handler = async(req, res) => {
  const dbStores = new Crud();
  const reqMethod = req.method
  let Store 
  try {
    if (!req.body) {

      console.error("El cuerpo de la solicitud está vacío.")
    }else{
      Store = JSON.parse(req.body)
    }

  } catch (error) {
    console.error("Error parsing JSON:", error);
  }  
  switch (reqMethod) {
    case "POST":
      if(Store){}
      const post = dbStores.postNewStore(Store)
      return res.status(201).json(post)
    case "GET":
      const get = dbStores.getShowStores()
      return res.status(200).json(get)
    case "PUT":
      let put 
      if(Store.name){
        //actualiza nombre de tienda
        put =dbStores.putInfoStore(Store)
        return res.status(200).json(put)
      }else{
        //actualiza los productos
        put = dbStores.putStore(Store)
        return res.status(200).json(put)
      }
    case "DELETE":
      let deleteS 
      console.log(Store+'Holaaa...')
      if(typeof Store !== 'number'){
        deleteS =dbStores.deleteProduct(Store)
      return res.status(200).json(deleteS)
      }else{
        deleteS = dbStores.deleteStore(Store)
      return res.status(200).json(deleteS)
      }
    default:
      res.status(200).json({ message: `Welcome to API of the Stores` });
  }
}
export default handler